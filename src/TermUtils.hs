module TermUtils
  ( paint,
    bold,
    italics,
    underline,
    Color (..),
  )
where

import Data.Text
import System.Console.ANSI

setSGRCode' :: [SGR] -> Text
setSGRCode' = pack . setSGRCode

reset :: Text
reset = setSGRCode' [Reset]

paint :: Color -> Text -> Text
paint color txt = setColor color <> txt <> reset
  where
    setColor c = setSGRCode' [SetColor Foreground Dull c]

bold :: Text -> Text
bold txt = setBold <> txt <> reset
  where
    setBold = setSGRCode' [SetConsoleIntensity BoldIntensity]

italics :: Text -> Text
italics txt = setItalics <> txt <> reset
  where
    setItalics = setSGRCode' [SetItalicized True]

underline :: Text -> Text
underline txt = setUnderline <> txt <> reset
  where
    setUnderline = setSGRCode' [SetUnderlining SingleUnderline]
