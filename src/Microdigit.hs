module Microdigit (microdigitizeSuperscript, microdigitizeSubscript) where

import Control.Monad
import Data.Map.Strict as M
import qualified Data.Text as T

digitsSuperscript :: Map Char Char
digitsSuperscript =
  M.fromList
    [ ('0', '⁰'),
      ('1', '¹'),
      ('2', '²'),
      ('3', '³'),
      ('4', '⁴'),
      ('5', '⁵'),
      ('6', '⁶'),
      ('7', '⁷'),
      ('8', '⁸'),
      ('9', '⁹'),
      ('+', '⁺'),
      ('-', '⁻'),
      ('=', '⁼'),
      ('(', '⁽'),
      (')', '⁾')
    ]

digitsSubscript :: Map Char Char
digitsSubscript =
  M.fromList
    [ ('0', '₀'),
      ('1', '₁'),
      ('2', '₂'),
      ('3', '₃'),
      ('4', '₄'),
      ('5', '₅'),
      ('6', '₆'),
      ('7', '₇'),
      ('8', '₈'),
      ('9', '₉'),
      ('+', '₊'),
      ('-', '₋'),
      ('=', '₌'),
      ('(', '₍'),
      (')', '₎')
    ]

microdigitize :: Map Char Char -> T.Text -> Maybe T.Text
microdigitize charMap txt =
  let chars = T.unpack txt
      microdigitizeChar c = M.lookup c charMap
      microdigitizedChars :: Maybe [Char] = forM chars microdigitizeChar
   in T.pack <$> microdigitizedChars

microdigitizeSuperscript :: T.Text -> Maybe T.Text
microdigitizeSuperscript = microdigitize digitsSuperscript

microdigitizeSubscript :: T.Text -> Maybe T.Text
microdigitizeSubscript = microdigitize digitsSubscript