-- {-# OPTIONS_GHC -Wno-orphans #-}

module SerDe where

import Data.Attoparsec.Text
import Data.Set as Set
import Data.Text as T
import Utils

class Serialize a where
  serialize :: a -> Text

class Parse a where
  parser :: Parser a

instance Serialize Text where
  serialize txt = txt

instance (Serialize a) => Serialize (Maybe a) where
  serialize (Just a) = serialize a
  serialize Nothing = ""

instance (Serialize a) => Serialize [a] where
  serialize = displayListWith serialize

instance (Serialize a) => Serialize (Set.Set a) where
  serialize = displayListWith serialize . Set.toList

instance Serialize Int where
  serialize = T.pack . show

instance Serialize Integer where
  serialize = T.pack . show