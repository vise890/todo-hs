module TodoFilesIO
  ( getTodoTxtFilePath,
    getDoneTxtFilePath,
    readTodoTxt,
    appendToTodoTxt,
    writeTodoTxt,
    readDoneTxt,
    appendToDoneTxt,
  )
where

import IDedList
import Persistence
import System.Directory
import System.Environment (getEnv)
import System.FilePath
import Todo.Todo

data TodoTxtPaths = TodoTxtPaths
  { todoPath :: FilePath,
    donePath :: FilePath
  }

localTodoPaths :: TodoTxtPaths
localTodoPaths =
  TodoTxtPaths {todoPath = "./todo.txt", donePath = "./done.txt"}

envTodoPaths :: IO TodoTxtPaths
envTodoPaths = do
  todoDir <- getEnv "TODO_DIR"
  return
    TodoTxtPaths
      { todoPath = todoDir </> "todo.txt",
        donePath = todoDir </> "done.txt"
      }

getTodoTxtPaths :: IO TodoTxtPaths
getTodoTxtPaths = do
  local <- doesFileExist (todoPath localTodoPaths)
  if local then return localTodoPaths else envTodoPaths

getTodoTxtFilePath :: IO FilePath
getTodoTxtFilePath = fmap todoPath getTodoTxtPaths

getDoneTxtFilePath :: IO FilePath
getDoneTxtFilePath = fmap donePath getTodoTxtPaths

readTodoTxt :: IO (IDedList Todo)
readTodoTxt = getTodoTxtFilePath >>= readListFile

appendToTodoTxt :: IDedList Todo -> IO ()
appendToTodoTxt todos = getTodoTxtFilePath >>= appendToListFile todos

writeTodoTxt :: IDedList Todo -> IO ()
writeTodoTxt todos = getTodoTxtFilePath >>= writeListFile todos

readDoneTxt :: IO (IDedList Todo)
readDoneTxt = getDoneTxtFilePath >>= readListFile

appendToDoneTxt :: IDedList Todo -> IO ()
appendToDoneTxt todos = getDoneTxtFilePath >>= appendToListFile todos
