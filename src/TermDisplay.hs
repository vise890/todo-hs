module TermDisplay
  ( TermDisplay,
    termDisplay,
    strikethrough,
  )
where

import Data.Text as T
import Utils

class TermDisplay a where
  termDisplay :: a -> Text

instance TermDisplay Text where
  termDisplay txt = txt

instance (TermDisplay a) => TermDisplay (Maybe a) where
  termDisplay (Just a) = termDisplay a
  termDisplay Nothing = ""

instance (TermDisplay a) => TermDisplay [a] where
  termDisplay = displayListWith termDisplay

strikethrough :: Text -> Text
strikethrough =
  (`T.snoc` strokeOverlay) . T.intersperse strokeOverlay
  where
    strokeOverlay = '\x0336'
