module Utils
  ( parseOnly',
    skipHorizontalSpace,
    skipHorizontalSpace1,
    displayListWith,
  )
where

import qualified Data.Attoparsec.Text as A
import qualified Data.Text as T

unwrap :: Either String a -> a
unwrap (Left err) = error err
unwrap (Right x) = x

parseOnly' :: A.Parser a -> T.Text -> a
parseOnly' p = unwrap . A.parseOnly (p <* A.endOfInput)

skipHorizontalSpace' :: (A.Parser Char -> t) -> t
skipHorizontalSpace' skipF =
  let horizontalCharParser = A.satisfy A.isHorizontalSpace
   in skipF horizontalCharParser

skipHorizontalSpace :: A.Parser ()
skipHorizontalSpace = skipHorizontalSpace' A.skipMany A.<?> "skipHorizontalSpace"

skipHorizontalSpace1 :: A.Parser ()
skipHorizontalSpace1 = skipHorizontalSpace' A.skipMany1 A.<?> "skipHorizontalSpace1"

displayListWith :: (a -> T.Text) -> [a] -> T.Text
displayListWith f =
  T.intercalate " " . remove T.null . fmap (T.strip . f)
  where
    remove f' = filter (not . f')