module Todo.Priority
  ( Priority (..),
    serialize,
    parser,
  )
where

import Data.Attoparsec.Text
import qualified Data.Char as C
import qualified Data.Text as T
import SerDe
import TermDisplay
import TermUtils

newtype Priority
  = Priority Char
  deriving (Eq, Show)

instance Ord Priority where
  compare (Priority p1) (Priority p2) = flip compare p1 p2

instance Serialize Priority where
  serialize (Priority pc) = "(" <> (T.toUpper . T.singleton $ pc) <> ")"

instance Parse Priority where
  parser = parserPriority

parserPriority :: Parser Priority
parserPriority =
  parserPriority' <?> "parserPriority"
  where
    parserPriority' = do
      _ <- "("
      p <- satisfy C.isAlpha
      _ <- ")"
      return $ Priority $ C.toUpper p

instance TermDisplay Priority where
  termDisplay = termDisplayPriority

termDisplayPriority :: Priority -> T.Text
termDisplayPriority p =
  case p of
    (Priority 'A') -> paint Red "(A)"
    (Priority 'B') -> paint Yellow "(B)"
    (Priority 'C') -> paint Green "(C)"
    (Priority 'D') -> paint Blue "(D)"
    (Priority _) -> serialize p
