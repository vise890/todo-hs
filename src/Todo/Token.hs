module Todo.Token
  ( Token (..),
    isContext,
    isProject,
    isThresholdDate,
    date,
    parser,
    termDisplay,
  )
where

import Control.Applicative
import Data.Attoparsec.Text
import Data.Char as Char
import Data.Text as T
import SerDe
import TermDisplay
import TermUtils
import Todo.Date as TD

data Token
  = Word Text
  | Context Text
  | Project Text
  | ThresholdDate TD.Date
  deriving (Eq, Ord, Show)

isContext :: Token -> Bool
isContext (Context _) = True
isContext _ = False

isProject :: Token -> Bool
isProject (Project _) = True
isProject _ = False

isThresholdDate :: Token -> Bool
isThresholdDate (ThresholdDate _) = True
isThresholdDate _ = False

date :: Token -> Maybe TD.Date
date (ThresholdDate d) = Just d
date _ = Nothing

instance Parse Token where
  parser = tokenParser

instance Serialize Token where
  serialize = displayToken

instance TermDisplay Token where
  termDisplay = termDisplayToken

tokenParser :: Parser Token
tokenParser =
  do
    ( (contextParser <?> "contextParser")
        <|> (projectParser <?> "projectParser")
        <|> (thresholdDateParser <?> "thresholdParser")
        <|> (wordParser <?> "wordParser")
      )
      <?> "tokenParser"
  where
    contextParser = do
      _ <- string "@"
      txt <- token
      return $ Context txt
    projectParser = do
      _ <- string "+"
      txt <- token
      return $ Project txt
    thresholdDateParser = do
      _ <- string "t:"
      d <- parser
      return $ ThresholdDate d
    wordParser = do
      txt <- token
      return $ Word txt

displayToken :: Token -> Text
displayToken (Word t) = t
displayToken (Context t) = "@" <> t
displayToken (Project t) = "+" <> t
displayToken (ThresholdDate d) = "t:" <> (serialize d)

termDisplayToken :: Token -> Text
termDisplayToken tkn@(Word _) = displayToken tkn
termDisplayToken tkn = paint Red $ displayToken tkn

isntSpace :: Char -> Bool
isntSpace = not . Char.isSpace

token :: Parser T.Text
token = takeWhile1 isntSpace