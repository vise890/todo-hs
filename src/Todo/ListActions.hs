module Todo.ListActions where

import qualified Data.Set as Set
import Data.Text (Text)
import IDedList
  ( ID,
    IDedList,
    update,
  )
import qualified IDedList as IDL
import qualified Todo.Date as TD
import qualified Todo.Todo as T
import qualified Todo.Token as Token

type ListUpdateFn = [ID] -> IDedList T.Todo -> IDedList T.Todo

complete :: TD.Date -> ListUpdateFn
complete completionDate = update $ T.complete completionDate

uncomplete :: ListUpdateFn
uncomplete = update T.uncomplete

prioritise :: Char -> ListUpdateFn
prioritise priority = update (T.prioritise . T.Priority $ priority)

unprioritise :: ListUpdateFn
unprioritise = update T.unprioritise

append :: Text -> ListUpdateFn
append postfix = update (T.append postfix)

prepend :: Text -> ListUpdateFn
prepend prefix = update (T.prepend prefix)

delete :: ListUpdateFn
delete = IDL.delete

-- FIXME: implement Traversable on IDedList to get these for free...
archive :: IDedList T.Todo -> (IDedList T.Todo, IDedList T.Todo)
archive ts = (compactedTodo, compactedCompleted)
  where
    (completed, todo) = IDL.partition T.isComplete ts
    compactedCompleted = compact completed
    compactedTodo = compact todo

compact :: IDedList T.Todo -> IDedList T.Todo
compact = IDL.compact

listContexts :: IDedList T.Todo -> Set.Set Token.Token
listContexts xs = foldl1 Set.union contexts
  where
    contexts = fmap T.contexts $ IDL.toList xs

listProjects :: IDedList T.Todo -> Set.Set Token.Token
listProjects xs = foldl1 Set.union projects
  where
    projects = fmap T.projects $ IDL.toList xs
