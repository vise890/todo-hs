module Todo.ParserUtils where

import Data.Attoparsec.Text as AP
import qualified Data.Text as Text

-- TODO: merge this into utils / simplify
restOfLine :: Parser Text.Text
restOfLine = AP.takeTill isEndOfLine

doesParse :: Parser a -> Parser Bool
doesParse p = option False (const True <$> p)
