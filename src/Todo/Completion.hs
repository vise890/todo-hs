module Todo.Completion (Completion (..), parser) where

import Data.Attoparsec.Text
import SerDe
import TermDisplay
import Todo.ParserUtils as PU

data Completion = Complete | Incomplete
  deriving (Eq, Show)

instance Serialize Completion where
  serialize Complete = "x"
  serialize Incomplete = ""

instance Parse Completion where
  parser = parserCompletion

parserCompletion :: Parser Completion
parserCompletion =
  parserCompletion' <?> "parserCompletion"
  where
    parserCompletion' = do
      c <- doesParse $ string "x "
      return $ if c then Complete else Incomplete

instance TermDisplay Completion where
  termDisplay = serialize
