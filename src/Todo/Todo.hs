{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Todo.Todo
  ( Priority (..),
    Todo,
    Todo.Todo.contexts,
    addCompletionDateIfNonePresent,
    addCreationDateIfNonePresent,
    append,
    complete,
    completion,
    completionDate,
    creationDate,
    isComplete,
    isActive,
    isInactive,
    isBlocked,
    isMaybe,
    isDelegated,
    isInInbox,
    isPrioritised,
    parser,
    prepend,
    prioritise,
    priority,
    projects,
    thresholdDate,
    thresholdDateIsPassed,
    tokens,
    uncomplete,
    unprioritise,
  )
where

import Control.Applicative
import Data.Attoparsec.Text
import Data.Maybe (isJust, listToMaybe)
import qualified Data.Set as Set
import Data.Text (Text)
import SerDe
import TermDisplay
import TermUtils
import Todo.Completion
import Todo.Date as TD
import Todo.Priority
import Todo.Token
import Utils

data Todo = Todo
  { completion :: Completion,
    priority :: Maybe Priority,
    completionDate :: Maybe TD.Date,
    creationDate :: Maybe TD.Date,
    thresholdDate :: Maybe TD.Date,
    tokens :: [Token],
    projects :: Set.Set Token,
    contexts :: Set.Set Token
  }
  deriving (Eq, Show)

-- TODO: just move to lenses

-- NOTE: used when tokens is shadowed by RecordsWildCards
tokens' :: Todo -> [Token]
tokens' = tokens

-- NOTE: used when creationDate is shadowed by RecordsWildCards
creationDate' :: Todo -> Maybe Date
creationDate' = creationDate

-- NOTE: used when completionDate is shadowed by RecordsWildCards
completionDate' :: Todo -> Maybe Date
completionDate' = completionDate

instance Ord Todo where
  compare t1 t2 = compare (sortKey t1) (sortKey t2)
    where
      sortKey t =
        ( not $ isComplete t,
          priority t,
          thresholdDate t,
          completionDate t,
          creationDate t,
          tokens t
        )

instance Serialize Todo where
  serialize = serializeTodo

instance Parse Todo where
  parser = parserTodo

instance TermDisplay Todo where
  termDisplay = termDisplayTodo

serializeTodo :: Todo -> Text
serializeTodo Todo {..} =
  serialize
    [ serialize completion,
      serialize priority,
      serialize completionDate,
      serialize creationDate,
      serialize tokens
    ]

parserTodo :: Parser Todo
parserTodo =
  parserTodo' <?> "parserTodo"
  where
    parserTodo' = do
      c <- parser
      p <- optional $ parser <* skipHorizontalSpace1
      dates <- optional $ eitherP parserBothDates parserCreationDate
      tkns <- parser `sepBy` skipHorizontalSpace1
      skipHorizontalSpace
      let todo =
            Todo
              { completion = c,
                completionDate = completionDate' dates,
                creationDate = creationDate' dates,
                priority = p,
                tokens = tkns,
                projects = Set.fromList $ filter isProject tkns,
                contexts = Set.fromList $ filter isContext tkns,
                thresholdDate = thresholdDate' tkns
              }
      if (isEmpty todo) -- FIXME: can we just fail if the line is just empty horizontal space?
        then fail "emptyTodo"
        else return todo

    parserBothDates :: Parser (TD.Date, TD.Date)
    parserBothDates = do
      d1 <- parser
      skipHorizontalSpace1
      d2 <- parser
      skipHorizontalSpace1
      return (d1, d2)

    parserCreationDate :: Parser TD.Date
    parserCreationDate = parser <* skipHorizontalSpace1

    completionDate' Nothing = Nothing
    completionDate' (Just (Left (d, _))) = Just d
    completionDate' (Just (Right _d)) = Nothing

    creationDate' Nothing = Nothing
    creationDate' (Just (Left (_, d))) = Just d
    creationDate' (Just (Right d)) = Just d

    thresholdDate' tkns = listToMaybe (filter isThresholdDate tkns) >>= date

isEmpty :: Todo -> Bool
isEmpty Todo {..} =
  completion == Incomplete
    && completionDate == Nothing
    && creationDate == Nothing
    && priority == Nothing
    && null tokens

termDisplayTodo :: Todo -> Text
termDisplayTodo t@Todo {..}
  | isComplete t = paint Black . strikethrough $ serializeTodo t
  | otherwise = termDisplay [termDisplay priority, termDisplay tokens]

isComplete :: Todo -> Bool
isComplete t =
  case (completion t) of
    Complete -> True
    Incomplete -> False

complete :: TD.Date -> Todo -> Todo
complete completionDate' t@Todo {..}
  | isComplete t = t
  | otherwise =
    let completion = Complete
        t' = Todo {..}
     in addCompletionDateIfNonePresent completionDate' t'

uncomplete :: Todo -> Todo
uncomplete Todo {..} =
  let completion = Incomplete
      completionDate = Nothing
   in Todo {..}

isPrioritised :: Todo -> Bool
isPrioritised t = isJust $ priority t

prioritise :: Priority -> Todo -> Todo
prioritise p Todo {..} =
  let priority = Just p
   in Todo {..}

unprioritise :: Todo -> Todo
unprioritise Todo {..} =
  let priority = Nothing
   in Todo {..}

prepend :: Text -> Todo -> Todo
prepend txt t@Todo {..} =
  let tokens = (Word txt) : tokens' t
   in Todo {..}

append :: Text -> Todo -> Todo
append txt t@Todo {..} =
  let tokens = tokens' t <> [(Word txt)]
   in Todo {..}

addCompletionDateIfNonePresent :: TD.Date -> Todo -> Todo
addCompletionDateIfNonePresent d t@Todo {..} =
  let completionDate = completionDate' t <|> Just d
      creationDate = creationDate' t <|> Just d
   in Todo {..}

addCreationDateIfNonePresent :: TD.Date -> Todo -> Todo
addCreationDateIfNonePresent d t@Todo {..} =
  let creationDate = creationDate' t <|> Just d
   in Todo {..}

hasContext :: Token -> Todo -> Bool
hasContext ctx t = ctx `elem` contexts t

isBlocked :: Todo -> Bool
isBlocked = hasContext (Context "@blocked")

isMaybe :: Todo -> Bool
isMaybe = hasContext (Context "@maybe")

isDelegated :: Todo -> Bool
isDelegated = hasContext (Context "@delegated")

isInactive :: TD.Date -> Todo -> Bool
isInactive threshold t =
  isBlocked t || isMaybe t || isDelegated t || not thresholdPassed
  where
    thresholdPassed = thresholdDateIsPassed threshold t

isActive :: TD.Date -> Todo -> Bool
isActive threshold = not . isInactive threshold

thresholdDateIsPassed :: TD.Date -> Todo -> Bool
thresholdDateIsPassed (TD.Date threshold) t =
  case thresholdDate t of
    Just (TD.Date d) -> d <= threshold
    Nothing -> True

isInInbox :: TD.Date -> Todo -> Bool
isInInbox today t =
  hasNoContexts && isActive'
  where
    hasNoContexts = null $ Todo.Todo.contexts t
    isActive' = isActive today t
