module Todo.Date
  ( Date (..),
    getCurrentDate,
  )
where

import Control.Applicative
import Data.Attoparsec.Text
import qualified Data.Text as T
import qualified Data.Time.Calendar as TC
import qualified Data.Time.Clock as TCl
import qualified Data.Time.Format as TF
import SerDe
import TermDisplay

defaultDayFormatString :: String
defaultDayFormatString = "%F"

serializeDay :: TC.Day -> T.Text
serializeDay =
  T.pack . TF.formatTime TF.defaultTimeLocale defaultDayFormatString

parserDay :: Parser TC.Day
parserDay = do
  s <- many1 (char '-' <|> digit)
  TF.parseTimeM False TF.defaultTimeLocale defaultDayFormatString s

newtype Date = Date TC.Day deriving (Eq, Ord, Show)

parserDate :: Parser Date
parserDate =
  parserDate' <?> "parserDate"
  where
    parserDate' = Date <$> parserDay

instance Serialize Date where
  serialize (Date d) = serializeDay d

instance Parse Date where
  parser = parserDate

instance TermDisplay Date where
  termDisplay = serialize

getCurrentDate :: IO Date
getCurrentDate = fmap (Date . TCl.utctDay) TCl.getCurrentTime