{-# LANGUAGE DeriveFoldable #-}

module IDedList
  ( ID,
    IDElem,
    IDedList (..),
    update,
    delete,
    sort,
    display,
    displayBare,
    fromList,
    toList,
    compact,
    getElems,
    IDedList.filter,
    partition,
  )
where

import Control.Applicative
import qualified Control.Arrow as Arrow
import qualified Data.Attoparsec.Text as A
import qualified Data.List as L
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import SerDe
import TermDisplay as TD
import qualified Text.Printf as Printf
import qualified Utils as U
import Prelude as P

type ID = Integer

type IDElem a = (ID, a)

-- TODO: turn this into a Map?
newtype IDedList a
  = IDedList [IDElem a]
  deriving (Eq, Ord, Foldable, Show)

instance Semigroup (IDedList a) where
  (IDedList xs) <> (IDedList ys) = IDedList $ xs <> ys

instance Monoid (IDedList a) where
  mempty = IDedList []

instance Functor IDedList where
  fmap = IDedList.map

instance (Serialize a) => Serialize (IDedList a) where
  serialize = serializeIDedList

instance (Parse a) => Parse (IDedList a) where
  parser = parserIDedList

instance (TermDisplay a) => TermDisplay (IDedList a) where
  termDisplay = IDedList.termDisplay

serializeIDedList :: (Serialize a) => (IDedList a) -> T.Text
serializeIDedList (IDedList []) = ""
serializeIDedList (IDedList xs) = T.unlines $ P.map itemOrBlankLine lineNumbers
  where
    ids = P.map fst xs
    fileLength = maximum ids
    lineNumbers = [1 .. fileLength]
    serializedItems :: Map.Map ID T.Text
    serializedItems = Map.fromList $ P.map (Arrow.second serialize) xs
    itemOrBlankLine :: ID -> T.Text
    itemOrBlankLine i = fromMaybe "" (Map.lookup i serializedItems)

parserIDedList :: (Parse a) => (A.Parser (IDedList a))
parserIDedList = do
  xs <- (a <|> blank) `A.sepBy` A.endOfLine
  A.skipSpace
  A.endOfInput
  let ixs = [(i, x) | (i, Just x) <- zip [1 ..] xs]
  return . IDedList $ ixs
  where
    blank = U.skipHorizontalSpace *> return Nothing
    a = do
      _ <- U.skipHorizontalSpace
      a' <- parser
      _ <- U.skipHorizontalSpace
      return $ Just a'

toList :: IDedList a -> [a]
toList (IDedList xs) = P.map snd xs

fromList :: [a] -> IDedList a
fromList = IDedList . zip [1 ..]

-- | Recomputes IDs
compact :: IDedList a -> IDedList a
compact = fromList . toList

update :: (a -> a) -> [ID] -> IDedList a -> IDedList a
update f targetIDs (IDedList xs) = IDedList $ ifMap needsUpdate xs
  where
    needsUpdate (i, _) = i `elem` targetIDs
    f' (i, x) = (i, f x)
    ifMap p = foldl (\acc x -> if p x then f' x : acc else x : acc) []

remove :: (a -> Bool) -> [a] -> [a]
remove f = P.filter (not . f)

delete :: [ID] -> IDedList a -> IDedList a
delete ids (IDedList xs) = IDedList $ remove ((`elem` ids) . fst) xs

sort :: Ord a => IDedList a -> IDedList a
sort (IDedList xs) = IDedList $ L.sortBy (\(_, x1) (_, x2) -> compare x1 x2) xs

getElems :: [ID] -> IDedList a -> IDedList a
getElems targetIDs = filterById (`elem` targetIDs)

filter :: (a -> Bool) -> IDedList a -> IDedList a
filter p (IDedList xs) = IDedList $ P.filter (p . snd) xs

filterById :: (ID -> Bool) -> IDedList a -> IDedList a
filterById p (IDedList xs) = IDedList $ P.filter (p . fst) xs

partition :: (a -> Bool) -> IDedList a -> (IDedList a, IDedList a)
partition p (IDedList xs) = (IDedList ls, IDedList rs)
  where
    (ls, rs) = L.partition (p . snd) xs

map :: (a -> b) -> IDedList a -> IDedList b
map _ (IDedList []) = IDedList []
map f (IDedList ((i, x) : xs)) =
  let h = (i, f x)
      t = fmap f (IDedList xs)
   in (IDedList [h]) <> t

showIDElem :: (a -> T.Text) -> IDElem a -> T.Text
showIDElem showFn (i, x) = showID i <> showFn x
  where
    showID = T.pack . Printf.printf "%3d "

display :: (Serialize a) => IDedList a -> T.Text
display (IDedList xs) = T.unlines [showIDElem serialize x | x <- xs]

termDisplay :: (TermDisplay a) => IDedList a -> T.Text
termDisplay (IDedList xs) = T.unlines [showIDElem TD.termDisplay x | x <- xs]

displayBare :: (Serialize a) => IDedList a -> T.Text
displayBare = T.unlines . fmap serialize . toList
