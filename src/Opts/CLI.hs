module Opts.CLI
  ( Cmd (..),
    optsParser,
  )
where

import qualified Data.Text as T
import Options.Applicative

type ID = Integer

data Cmd
  = Add T.Text
  | Append
      T.Text
      [ID]
  | Archive Bool
  | Clean
  | Complete [ID]
  | Edit
  | List Bool
  | Prepend
      T.Text
      [ID]
  | Prioritise
      Char
      [ID]
  | Remove [ID]
  | Uncomplete [ID]
  | Unprioritise [ID]
  | ListContexts
  | ListProjects
  | ListInbox
  deriving (Show)

optsParser :: ParserInfo Cmd
optsParser =
  info
    (helper <*> optsParser')
    (fullDesc <> header "todo-hs - A todo.txt CLI manager")

optsParser' :: Parser Cmd
optsParser' =
  subparser $
    commands ["add", "a"] add
      <> commands ["append", "app"] append
      <> commands ["archive", "ar"] archive
      <> commands ["arr"] pirateArchive
      <> commands ["clean", "cl"] clean
      <> commands ["complete", "do"] complete
      <> commands ["edit", "e"] edit
      <> commands ["list", "ls"] list
      <> commands ["prepend", "prep"] prepend
      <> commands ["prioritise", "pri"] prioritise
      <> commands ["remove", "rm"] remove
      <> commands ["uncomplete", "undo"] uncomplete
      <> commands ["unprioritise", "unpri"] unprioritise
      <> commands ["contexts", "ctx", "ctxs"] listContexts
      <> commands ["projects", "pj", "pjs"] listProjects
      <> commands ["inbox", "in"] listInbox
  where
    ids =
      some $ argument auto (metavar "ID..." <> help "The IDs of the target todos")
    add =
      info
        ( helper
            <*> ( Add
                    <$> argument
                      str
                      ( metavar "TODO_TEXT"
                          <> help
                            "The text/body of the todo. PSST: quote and separate \
                            \todos with newlines to add multiple todos."
                      )
                )
        )
        (progDesc "Add todo(s)")
    append =
      info
        ( helper
            <*> ( Append
                    <$> argument
                      str
                      ( metavar "TEXT"
                          <> help "The text to append to the target todo(s)"
                      )
                    <*> ids
                )
        )
        (progDesc "Append TEXT to target todo(s)")
    archive =
      info (helper <*> pure (Archive False)) (progDesc "archive completed todos")
    pirateArchive = info (pure $ Archive True) (progDesc "archive in style")
    clean =
      info
        (helper <*> pure Clean)
        (progDesc "archive completed todos, sort and remove whitespace in todo.txt")
    complete =
      info
        (helper <*> (Complete <$> ids))
        (progDesc "Mark the target todo(s) as done")
    edit =
      info (helper <*> pure Edit) (progDesc "edit todo.txt file with $EDITOR")
    list =
      info
        ( helper
            <*> ( List
                    <$> switch
                      ( short 'a'
                          <> long "all"
                          <> help
                            "show all todos, including ones with threshold in \
                            \future and in ignored contexts"
                      )
                )
        )
        (progDesc "list all todos")
    prepend =
      info
        ( helper
            <*> ( Prepend
                    <$> argument
                      str
                      ( metavar "TEXT"
                          <> help "The text to prepend to the target todo(s)"
                      )
                    <*> ids
                )
        )
        (progDesc "Prepend TEXT to target todo(s)")
    prioritise =
      info
        ( helper
            <*> ( Prioritise
                    <$> argument
                      (fmap head str) -- FIXME: this is naughty, but i wanna sleep
                      (metavar "PRIORITY" <> help "A priority (e.g. A, B, C, etc.)")
                    <*> ids
                )
        )
        (progDesc "Give PRIORITY to target todo(s)")
    remove =
      info (helper <*> (Remove <$> ids)) (progDesc "Delete target todo(s)")
    uncomplete =
      info
        (helper <*> (Uncomplete <$> ids))
        (progDesc "Mark target todo(s) as not done (to do)")
    unprioritise =
      info
        (helper <*> (Unprioritise <$> ids))
        (progDesc "Remove priority from target todo(s)")
    listContexts =
      info (helper <*> pure ListContexts) (progDesc "List all the contexts")
    listProjects =
      info (helper <*> pure ListProjects) (progDesc "List all the projects")
    listInbox =
      info
        (helper <*> pure ListInbox)
        (progDesc "List todos in the inbox (i.e. todos without a context)")

commands :: [String] -> ParserInfo a -> Mod CommandFields a
commands cmdAliases pinfo =
  foldr (\cmd acc -> acc <> command cmd pinfo) mempty cmdAliases
