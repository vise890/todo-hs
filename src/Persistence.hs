module Persistence where

import qualified Data.Text.IO as TIO
import IDedList
import SerDe
import System.Directory
  ( removeFile,
    renameFile,
  )
import System.IO
  ( hClose,
    openTempFile,
  )
import Utils

readListFile :: Parse a => FilePath -> IO (IDedList a)
readListFile path = do
  txt <- TIO.readFile path
  let pr = parseOnly' parser txt
  return pr

writeListFile :: Serialize a => IDedList a -> FilePath -> IO ()
writeListFile xs path = do
  let newContents = serialize xs
  (tempName, tempH) <- openTempFile "." "temp"
  TIO.hPutStr tempH newContents
  hClose tempH
  removeFile path
  renameFile tempName path

appendToListFile :: Serialize a => IDedList a -> FilePath -> IO ()
appendToListFile xs path = TIO.appendFile path $ serialize xs
