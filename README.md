# todo-hs

An alternative CLI app for the popular (well, among geeks anyway)
[Todo.txt](http://todotxt.com/) ecosystem.

Some improvements are:

* doesn’t mess up todo.txt, keeps todos where they are
  * maintains todo IDs across runs
* most commands take multiple `todiID`s (e.g., `t pri A 1 2 12 32 42` will
  prioritise all the todos given)
* uses the `todo.txt` in the current directory if there is one (useful for
  project-specific todos)

Mainly to teach myself haskell.

# Setup

```bash
git checkout https://github.com/vise890/todo.hs

# put the env var in your ~/.bashrc or ~/.zshrc:
echo 'export TODO_DIR="/path/to/todo-txt/dir/"' > ~/.zshrc

# optional (if you just wanna test it out):
stack build

stack install
export PATH=~/.local/bin:$PATH
```

# Usage

```bash
t add "(A) become ultra-zen"
t a "(B) go to easter island"

t prioritise A 1 2 3
t pri C 2 4

t complete 1 2 3 5
t do 11 1 10

t archive
t ar

...
# for more, read the dispatch function in Main.hs
```

# Testing

```bash
stack test
```
