(A) todosql support
(A) tui: todosql views listed in a sidebar
(A) autocompletion for cli https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/stack/stack.plugin.zsh
(B) add hidden task support as per https://github.com/mpcjanssen/simpletask-android/blob/master/app/src/main/assets/index.en.md
(C) add due dates as per https://github.com/mpcjanssen/simpletask-android/blob/master/app/src/main/assets/index.en.md
(C) grep and fix FIXME/TODO
2020-10-13 postpone todos by ~1w+/-1d, 1month+/-5days,etc. (randomish amounts)
add recurrence as per https://github.com/mpcjanssen/simpletask-android/blob/master/app/src/main/assets/index.en.md
archive arbitrary @contexts / +tags (e.g. +someday @ikea)
config file: ignored contexts
switch for global todo.txt
t ls (A)
