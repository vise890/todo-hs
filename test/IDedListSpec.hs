module IDedListSpec where

import Control.Applicative
import qualified Data.Attoparsec.Text as A
import qualified Data.Text as T
import IDedList
import SerDe
import Test.Hspec

data ABC
  = A
  | B
  | C
  deriving (Show, Eq, Ord)

instance Serialize ABC where
  serialize = T.pack . show

instance Parse ABC where
  parser =
    let p str abc = A.string str >> return abc
     in (p "A" A) <|> (p "B" B) <|> (p "C" C)

spec :: Spec
spec = do
  describe "fromList -> toList round trip" $ do
    it "goes back to the original list" $ do
      let xs :: [Int] = [1, 3, 5]
      let r = toList . fromList
      r xs `shouldBe` xs

  describe "sort" $ do
    it "uses the contents of the elements for sorting (ignoring ID)" $ do
      let idxs :: IDedList Int = fromList [3, 4, 2]
      let actual = sort idxs
      toList actual `shouldBe` [2, 3, 4]

  describe "display" $ do
    it "displays the items and IDs" $ do
      let idxs :: IDedList ABC = fromList [A, B, C]
      let actual = display idxs
      actual `shouldBe` "  1 A\n  2 B\n  3 C\n"

    it "pads IDs so that they are aligned" $ do
      let idxs :: IDedList Int = fromList [1 .. 100]
      let actual = T.lines $ display idxs
      (drop 8 . take 10 $ actual) `shouldBe` ["  9 9", " 10 10"]
      drop 98 actual `shouldBe` [" 99 99", "100 100"]

  describe "displayBare" $ do
    it "displays the items without their IDs" $ do
      let idxs :: IDedList ABC = fromList [A, B, C]
      let actual = displayBare idxs
      actual `shouldBe` "A\nB\nC\n"

  describe "Serialize instance" $ do
    it "serializes to text" $ do
      let xs = fromList [A, B, C]
      let got = serialize xs
      let expected = "A\nB\nC\n"
      got `shouldBe` expected

  describe "Parse instance" $ do
    it "parses lines with elems" $ do
      let input = "A\nB\nC\nB"
      let got = A.parseOnly parser input
      let expected = Right $ fromList [A, B, C, B]
      got `shouldBe` expected

    it "parses empty string" $ do
      let input = ""
      let p :: A.Parser (IDedList ABC) = parser
      let got = A.parseOnly p input
      let expected = Right $ fromList []
      got `shouldBe` expected

    it "parses empty string with EOL" $ do
      let input = "\n"
      let p :: A.Parser (IDedList ABC) = parser
      let got = A.parseOnly p input
      let expected = Right $ fromList []
      got `shouldBe` expected

    it "parses empty string with winzoz EOL" $ do
      let input = "\r\n"
      let p :: A.Parser (IDedList ABC) = parser
      let got = A.parseOnly p input
      let expected = Right $ fromList []
      got `shouldBe` expected

    it "parses elems with horizontal whitespace around them" $ do
      let input = " A\t \n  B\r\n C\nB"
      let got = A.parseOnly parser input
      let expected = Right $ fromList [A, B, C, B]
      got `shouldBe` expected

    it "parses empty lines" $ do
      let input = "  \n \t \n     "
      let p :: A.Parser (IDedList ABC) = parser
      let got = A.parseOnly p input
      let expected = Right $ fromList []
      got `shouldBe` expected

    it "skips blank lines, but preserves IDs" $ do
      let input = "A\nB\n \n \t \n\nC\nB\n  \r\n"
      let got = A.parseOnly parser input
      let expected = Right $ IDedList [(1, A), (2, B), (6, C), (7, B)]
      got `shouldBe` expected

    it "skips last blank line, if no EOL" $ do
      let input = "A\nB\n  "
      let got = A.parseOnly parser input
      let expected = Right $ IDedList [(1, A), (2, B)]
      got `shouldBe` expected

    it "skips last blank line, if EOL" $ do
      let input = "A\nB\n  \n"
      let got = A.parseOnly parser input
      let expected = Right $ IDedList [(1, A), (2, B)]
      got `shouldBe` expected

  describe "partition" $ do
    it "partitions the IDL" $ do
      let idl :: IDedList Integer = fromList [1, 2, 3]
      let got = partition odd idl
      let expected = (IDedList [(1, 1), (3, 3)], IDedList [(2, 2)])
      got `shouldBe` expected
