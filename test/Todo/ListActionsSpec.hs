module Todo.ListActionsSpec where

import qualified Data.Set as Set
import Data.Text as T
import IDedList as IDL
import SerDe
import Test.Hspec
import Todo.ListActions
import Todo.Todo (Todo)
import Todo.Token
import Utils

t :: Text -> Todo
t = parseOnly' parser

idl2set :: (Ord a) => IDedList a -> Set.Set a
idl2set = Set.fromList . IDL.toList

txts2idl :: [Text] -> IDedList Todo
txts2idl = parseOnly' parser . T.unlines

ts :: [Text] -> [Todo]
ts = IDL.toList . txts2idl

txts2set :: [Text] -> Set.Set Todo
txts2set = Set.fromList . ts

spec :: Spec
spec = do
  describe "archive" $ do
    it "separates the completed todos" $ do
      let (todo, done) = archive $ txts2idl ["foo", "x bar"]
      toList todo `shouldBe` ts ["foo"]
      toList done `shouldBe` ts ["x bar"]
    it "filters out blank todos" $ do
      let (todo, done) = archive $ txts2idl ["foo", "", "  ", "x bar"]
      todo `shouldBe` txts2idl ["foo"]
      done `shouldBe` txts2idl ["x bar"]

  describe "prepend" $ do
    let todos = prepend "foo" [2, 3] $ txts2idl ["bar", "x baz", "x (A) boop"]
    it "prepends text to the selected todos only" $ do
      let newTodoList = idl2set todos
      let expectedNewTodoList = txts2set ["bar", "x foo baz", "x (A) foo boop"]
      newTodoList `shouldBe` expectedNewTodoList

  describe "delete" $ do
    let todos = Todo.ListActions.delete [1, 3] $ txts2idl ["foo", "bar", "baz"]
    it "deletes selected todos" $ do
      let newTodos = idl2set todos
      let expected = txts2set ["bar"]
      newTodos `shouldBe` expected

  describe "prioritise" $ do
    let todos = prioritise 'A' [1, 3] $ txts2idl ["foo", "bar", "baz"]
    it "prioritises selected todos" $ do
      let newTodos = idl2set todos
      let expected = txts2set ["(A) foo", "bar", "(A) baz"]
      newTodos `shouldBe` expected

  describe "listContexts" $ do
    let todos = txts2idl ["@coo foo", "@coo bar", "@boo baz"]
    it "lists all contexts once" $ do
      let got = listContexts todos
      let expected = Set.fromList $ fmap Context ["coo", "boo"]
      got `shouldBe` expected

  describe "listProjects" $ do
    let todos = txts2idl ["+pro foo", "@coo bar", "@boo +pri baz"]
    it "lists all projects once" $ do
      let got = listProjects todos
      let expected = Set.fromList $ fmap Project ["pro", "pri"]
      got `shouldBe` expected
