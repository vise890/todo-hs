module Todo.DateSpec where

import Data.Text as T
import SerDe
import Test.Hspec
import Todo.Date
import Utils

parseDate :: T.Text -> Date
parseDate = parseOnly' parser

spec :: Spec
spec = do
  describe "SerDe" $ do
    it "roundtrips" $ do
      let str = "2019-01-01"
      let rt = serialize . parseDate $ str
      rt `shouldBe` str

    it "strips 0s" $ do
      let str = "0001-01-01"
      let rt = serialize . parseDate $ str
      rt `shouldBe` "1-01-01"
