module Todo.PrioritySpec where

import Data.Attoparsec.Text
import Data.Either
import Test.Hspec
import Todo.Priority
import Utils

spec :: Spec
spec = do
  describe "the Ord instance" $
    it "compares priorities by letter" $ do
      compare (Priority 'A') (Priority 'B') `shouldBe` GT
      compare (Priority 'B') (Priority 'A') `shouldBe` LT
      compare (Priority 'A') (Priority 'A') `shouldBe` EQ
  describe "the Serialize instance" $ do
    it "parenthesizes the priority" $ do
      serialize (Priority 'A') `shouldBe` "(A)"
  describe "the parser" $ do
    it "parses a valid string" $ do
      parseOnly' parser "(A)" `shouldBe` Priority 'A'
    it "parses lowercase letters" $ do
      parseOnly' parser "(a)" `shouldBe` Priority 'A'
    it "don't parse non-alpha chars valid string" $ do
      let got :: Either String Priority = parseOnly parser "(!)"
      isLeft got `shouldBe` True
