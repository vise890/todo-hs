{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Todo.TodoSpec where

import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Data.Time.Calendar as TC
import SerDe
import Test.Hspec
import Todo.Completion
import Todo.Date
import Todo.Todo
import Todo.Token
import Utils

parseTodo :: T.Text -> Todo
parseTodo = parseOnly' parser

spec :: Spec
spec = do
  describe "the Ord Instance" $ do
    it "ranks a completed todo lower than an uncompleted one" $ do
      compare (parseTodo "x (A) foo") (parseTodo "bar") `shouldBe` LT
    it "ranks an uncompleted todo higher than a completed one" $ do
      compare (parseTodo "bar") (parseTodo "x (A) foo") `shouldBe` GT
    it "orders unprioritised todos lexicographically (on todo text)" $ do
      compare (parseTodo "bbb") (parseTodo "aaa") `shouldBe` GT
      compare (parseTodo "x aaa") (parseTodo "x bbb") `shouldBe` LT
      compare (parseTodo "x ccc") (parseTodo "x ccc") `shouldBe` EQ
    it "ranks todos by priority if both have same completion status" $ do
      compare (parseTodo "(A) foo") (parseTodo "(B) bar") `shouldBe` GT
      compare (parseTodo "(B) foo") (parseTodo "(A) bar") `shouldBe` LT
      compare (parseTodo "x (A) foo") (parseTodo "x (B) bar") `shouldBe` GT
      compare (parseTodo "x (B) foo") (parseTodo "x (A) bar") `shouldBe` LT
    it "ranks todos completed earlier lower than todos completed later" $ do
      compare (parseTodo "x 2018-01-01 foo") (parseTodo "x 2019-01-01 foo")
        `shouldBe` LT
    it
      "ranks completed todos without completion date lower than ones with completion date"
      $ do
        compare (parseTodo "x foo") (parseTodo "x 2019-01-01 foo")
          `shouldBe` LT

  describe "the Parse instance" $ do
    it "parses valid todo strings" $ do
      priority (parseTodo "foo") `shouldBe` Nothing
      priority (parseTodo "(A) foo") `shouldBe` Just (Priority 'A')
      tokens (parseTodo "(A) foo") `shouldBe` [(Word "foo")]
      completion (parseTodo "foo") `shouldBe` Incomplete
    it "capitalizes the priority" $ do
      let t = parseTodo "(a) foo"
      priority t `shouldBe` Just (Priority 'A')
    describe "parsing dates" $ do
      let creationDate' = Date $ TC.fromGregorian 1 1 1
      it "when none are present" $ do
        let t = parseTodo "x foo"
        completionDate t `shouldBe` Nothing
        creationDate t `shouldBe` Nothing
      describe "when just the creation date is present" $ do
        it "when todo is completed" $ do
          let t = parseTodo "x 1-01-01 foo"
          completion t `shouldBe` Complete
          creationDate t `shouldBe` Just creationDate'
          completionDate t `shouldBe` Nothing
        it "when todo is not completed" $ do
          let t = parseTodo "1-01-01 foo"
          creationDate t `shouldBe` Just creationDate'
          completionDate t `shouldBe` Nothing
      describe "when both creation and completion date are present" $ do
        let completionDate' = Date $ TC.fromGregorian 2 2 2
        it "when todo is completed and prioritised" $ do
          let t = parseTodo "x (A) 2-02-02 1-01-01 foo"
          priority t `shouldBe` Just (Priority 'A')
          tokens t `shouldBe` [(Word "foo")]
          creationDate t `shouldBe` Just creationDate'
          completionDate t `shouldBe` Just completionDate'
        it "when todo is neither complete nor prioritised" $ do
          let t2 = parseTodo "2-02-02 1-01-01 foo"
          creationDate t2 `shouldBe` Just creationDate'
          completionDate t2 `shouldBe` Just completionDate'
  describe "serialize" $
    it "renders Todos correctly" $ do
      serialize (parseTodo "foo") `shouldBe` "foo"
      serialize (parseTodo "x foo") `shouldBe` "x foo"
      serialize (parseTodo "(a) foo") `shouldBe` "(A) foo"
      serialize (parseTodo "x (a) foo") `shouldBe` "x (A) foo"

  describe "isComplete" $ do
    it "is True if a todo is completed" $ do
      isComplete (parseTodo "x foo") `shouldBe` True
    it "is False if a todo is not completed" $ do
      isComplete (parseTodo "foo") `shouldBe` False

  describe "complete" $ do
    let someDate = Date $ TC.fromGregorian 2 2 2
    describe "when no creationDate is present" $ do
      it "completes the todo and sets creationDate == completionDate" $ do
        let got = complete someDate (parseTodo "(A) foo")
            exp = parseTodo "x (A) 2-02-02 2-02-02 foo"
        creationDate exp `shouldBe` Just someDate
        completionDate exp `shouldBe` Just someDate
        completionDate got `shouldBe` completionDate exp
        creationDate got `shouldBe` creationDate exp
        got `shouldBe` exp

    describe "when there already is a creationDate" $ do
      it "completes the todo and sets completionDate" $ do
        let expCreationDate = Date $ TC.fromGregorian 1 1 1
        let got = complete someDate (parseTodo "(A) 1-01-01 foo")
            exp = parseTodo "x (A) 2-02-02 1-01-01 foo"
        completionDate exp `shouldBe` Just someDate
        creationDate exp `shouldBe` Just expCreationDate
        got `shouldBe` exp
      it "does not change an already completed and dated todo" $ do
        complete someDate (parseTodo "x 3-03-03 1-01-01 foo")
          `shouldBe` parseTodo "x 3-03-03 1-01-01 foo"
      it "does not change an already completed" $ do
        complete someDate (parseTodo "x foo")
          `shouldBe` parseTodo "x foo"

  describe "uncomplete" $ do
    it "uncompletes a completed todo" $ do
      uncomplete (parseTodo "x foo") `shouldBe` parseTodo "foo"
    it "removes completion date from a completed todo" $ do
      uncomplete (parseTodo "x 2-02-02 1-01-01 foo")
        `shouldBe` parseTodo "1-01-01 foo"
    it "does not change an uncompleted todo" $ do
      uncomplete (parseTodo "foo") `shouldBe` parseTodo "foo"

  describe "priority" $ do
    it "is the priority for prioritised todos" $ do
      let tps = [("(A) foo", 'A'), ("(B) foo", 'B'), ("(C) foo", 'C')]
      let actual = map (priority . parseTodo . fst) tps
      let expected = map (Just . Priority . snd) tps
      actual `shouldBe` expected
    it "is None for an unprioritised todo" $ do
      priority (parseTodo "foo") `shouldBe` Nothing

  describe "isPrioritised" $ do
    it "is True for a isPrioritised todo" $ do
      isPrioritised (parseTodo "(A) foo") `shouldBe` True
    it "is False for an unprioritised todo" $ do
      isPrioritised (parseTodo "foo") `shouldBe` False

  describe "prioritise" $ do
    it "prioritises a todo" $ do
      let t = parseTodo "foo"
      prioritise (Priority 'B') t `shouldBe` parseTodo "(B) foo"
    it "prioritises an already prioritised todo" $ do
      let t = parseTodo "x (A) foo"
      prioritise (Priority 'B') t `shouldBe` parseTodo "x (B) foo"

  describe "unprioritise" $ do
    it "unprioritises a todo" $ do
      let t = parseTodo "x (B) foo"
      unprioritise t `shouldBe` parseTodo "x foo"
    it "does not change an unprioritised todo" $ do
      let t = parseTodo "x foo"
      unprioritise t `shouldBe` parseTodo "x foo"

  describe "append" $
    it "appends text at the end, with a space" $ do
      let t = parseTodo "x (A) boop"
      append "bop" t `shouldBe` parseTodo "x (A) boop bop"

  describe "prepend" $ do
    it "prepends text and a space" $ do
      let t = parseTodo "boop"
      prepend "beep" t `shouldBe` parseTodo "beep boop"
    it "prepends text after completion marker" $ do
      let t = parseTodo "x boo"
      prepend "foo" t `shouldBe` parseTodo "x foo boo"
    it "prepends text after priority marker" $ do
      let t = parseTodo "(A) boo"
      prepend "foo" t `shouldBe` parseTodo "(A) foo boo"
    it "in other words, it just prepends text after the marker things" $ do
      let t = parseTodo "x (A) boo"
      prepend "foo" t `shouldBe` parseTodo "x (A) foo boo"

  describe "addCreation+CompletionDateIfNotPresent" $ do
    describe "todos with both dates" $ do
      it "doesn't change them" $ do
        let d = Date $ TC.fromGregorian 3 3 3
        let t = parseTodo "2-02-02 1-01-01 boo"
        addCreationDateIfNonePresent d t `shouldBe` t
        addCompletionDateIfNonePresent d t `shouldBe` t
        let t2 = parseTodo "x 2-02-02 1-01-01 boo"
        addCreationDateIfNonePresent d t2 `shouldBe` t2
        addCompletionDateIfNonePresent d t2 `shouldBe` t2
        let t3 = parseTodo "x (A) 2-02-02 1-01-01 boo"
        addCreationDateIfNonePresent d t3 `shouldBe` t3
        addCompletionDateIfNonePresent d t3 `shouldBe` t3
    describe "todos with creation date only" $ do
      let d = Date $ TC.fromGregorian 2 2 2
      let t = parseTodo "x (A) 1-01-01 boo"
      it "does not change creation date" $ do
        addCreationDateIfNonePresent d t `shouldBe` t
      it "adds completion date" $ do
        addCompletionDateIfNonePresent d t
          `shouldBe` parseTodo "x (A) 2-02-02 1-01-01 boo"
    describe "todos with no dates" $ do
      let d = Date $ TC.fromGregorian 2 2 2
      let t = parseTodo "boo"
      it "adds creation date" $ do
        let exp = parseTodo "2-02-02 boo"
            got = addCreationDateIfNonePresent d t
        exp `shouldBe` got
      it "adds completion *and* creation date" $ do
        addCompletionDateIfNonePresent d t
          `shouldBe` parseTodo "2-02-02 2-02-02 boo"

  describe "contexts" $ do
    it "lists the contexts of the todo" $ do
      let t = parseTodo "(A) foo bar @ctx baz @ctx2 quz"
          expContexts = Set.fromList $ fmap Context ["ctx", "ctx2"]
      contexts t `shouldBe` expContexts
    it "does not consider '@' to be a context" $ do
      let t = parseTodo "meet Dr. X @ 9am on Monday"
          expContexts = Set.fromList []
      contexts t `shouldBe` expContexts

  describe "projects" $ do
    it "lists the projexts associated with the todo" $ do
      let t = parseTodo "(A) foo bar +pj1 baz +pj2 @ctx quz"
          expProjects = Set.fromList $ fmap Project ["pj1", "pj2"]
      projects t `shouldBe` expProjects
    it "does not consider '+' to be a project" $ do
      let t = parseTodo "write + send email"
          expProjects = Set.fromList []
      projects t `shouldBe` expProjects

  describe "isInInbox" $ do
    let td = Date $ TC.fromGregorian 2020 12 30
    it "returns true when a todo does not have a context" $ do
      let t = parseTodo "(A) foo bar +pj1 baz +pj2 quz"
      isInInbox td t `shouldBe` True
    it "returns false when a todo's threshold date is beyond the given date" $ do
      let t = parseTodo "(A) t:2030-12-20 foo bar"
      isInInbox td t `shouldBe` False
      let t2 = parseTodo "(A) t:2030-12-20 @foo bar"
      isInInbox td t2 `shouldBe` False

  describe "thresholdDate" $ do
    it "returns Nothing when a todo does not have a threshold date" $ do
      let t = parseTodo "(A) foo bar @baz +foo "
      thresholdDate t `shouldBe` Nothing
    it "returns Just d when a todo does not have a threshold date" $ do
      let t = parseTodo "(A) foo bar @baz +foo t:2020-12-30"
      let expected = Just . Date $ TC.fromGregorian 2020 12 30
      thresholdDate t `shouldBe` expected

  describe "thresholdDateIsPassed" $ do
    let targetThreshold = Date $ TC.fromGregorian 2020 2 2
    it "returns True when a todo does not have a threshold date" $ do
      let t = parseTodo "(A) foo bar @baz +foo "
      thresholdDateIsPassed targetThreshold t `shouldBe` True
    it "returns True when a todo has a threshold date == targetThreshold" $ do
      let t = parseTodo "(A) foo bar @baz +foo t:2020-02-02"
      thresholdDateIsPassed targetThreshold t `shouldBe` True
    it "returns True when a todo has a threshold date <= targetThreshold" $ do
      let t = parseTodo "(A) foo bar @baz +foo t:2020-01-01"
      thresholdDateIsPassed targetThreshold t `shouldBe` True

  describe "isInactive" $ do
    let targetThreshold = Date $ TC.fromGregorian 2020 2 2
    it "returns False when a todo does not have a threshold date, nor is it contexted with @@blocked or @@maybe" $ do
      let t = parseTodo "(A) foo"
      isInactive targetThreshold t `shouldBe` False
    it "returns False when a todo has a threshold date, and it is passed" $ do
      let t = parseTodo "(A) foo t:2020-01-01"
      thresholdDateIsPassed targetThreshold t `shouldBe` True
      isInactive targetThreshold t `shouldBe` False
    it "returns True when a todo has a threshold date, and it is not passed" $ do
      let t = parseTodo "(A) foo t:2020-03-03"
      thresholdDateIsPassed targetThreshold t `shouldBe` False
      isInactive targetThreshold t `shouldBe` True
    it "returns True when a todo is contexted with @@blocked" $ do
      let t = parseTodo "(A) foo @@blocked"
      isInactive targetThreshold t `shouldBe` True
    it "returns True when a todo is contexted with @@maybe" $ do
      let t = parseTodo "(A) foo @@maybe"
      isInactive targetThreshold t `shouldBe` True

  describe "roundtripping degenerate todos yields same/stripped strings" $ do
    let shouldRoundtrip todoTxt = (serialize . parseTodo $ todoTxt) `shouldBe` todoTxt
        shouldRoundtrip' todoTxt expectedTxt = (serialize . parseTodo $ todoTxt) `shouldBe` expectedTxt
    it "" $ do
      shouldRoundtrip "x"
      shouldRoundtrip' "x " "x"
      shouldRoundtrip "(A)"
      shouldRoundtrip' "(A)  \t  \t" "(A)"
      shouldRoundtrip "x (A)"
      shouldRoundtrip' "x (A)  " "x (A)"
