{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Todo.TodoListSpec where

import qualified Data.Text as T
import IDedList
import SerDe
import Test.Hspec
import Todo.ListActions as IDL
import Todo.Todo
import Utils

t :: T.Text -> Todo
t = parseOnly' parser

txts2idl :: [T.Text] -> IDedList Todo
txts2idl = parseOnly' parser . T.unlines

spec :: Spec
spec = do
  describe "SerDe" $ do
    describe "serialize" $ do
      it "serializes to text" $ do
        let got = serialize $ txts2idl ["a", "b"]
        let exp = "a\nb\n"
        got `shouldBe` exp

    describe "parser" $ do
      it "parses from text" $ do
        let got = parseOnly' parser "a\nb"
        let exp = txts2idl ["a", "b"]
        got `shouldBe` exp

      it "skips blank lines" $ do
        let got = parseOnly' parser "a\n \n\n  \t \nb\n\n"
        let exp = IDedList [(1, t "a"), (5, t "b")]
        got `shouldBe` exp

    describe "persisting IDs" $ do
      it "should preserve IDs across runs" $ do
        let start = "a\n \n \nb\nc"
        let got = parseOnly' parser start
        let exp = IDedList [(1, t "a"), (4, t "b"), (5, t "c")]
        got `shouldBe` exp
        let deleteA = IDL.delete [1] exp
        let exp2 = IDedList [(4, t "b"), (5, t "c")]
        deleteA `shouldBe` exp2
        let serialized2 = serialize exp2
        let got2 = parseOnly' parser serialized2
        got2 `shouldBe` exp2
