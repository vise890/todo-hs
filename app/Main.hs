module Main where

import Control.Monad (when)
import qualified Data.Char as Char
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import IDedList
  ( ID,
    IDedList,
  )
import qualified IDedList as IDL
import Options.Applicative
import qualified Opts.CLI as CLI
import SerDe
import qualified System.Environment as Env
import qualified System.Process as Process
import TermDisplay
import TermUtils
import qualified Todo.Date as TD
import qualified Todo.ListActions as LA
import qualified Todo.Todo as T
import TodoFilesIO
import Utils

displayTodos :: IDedList T.Todo -> IO ()
displayTodos = TIO.putStr . termDisplay . IDL.sort

list :: Bool -> IO ()
list listAll = do
  todos <- readTodoTxt
  today <- TD.getCurrentDate
  let todos' =
        if listAll
          then todos
          else
            IDL.filter
              (T.isActive today)
              todos
  displayTodos todos'

listContexts :: IO ()
listContexts = do
  todos <- readTodoTxt
  let cxts = LA.listContexts todos
  TIO.putStr $ serialize cxts

listProjects :: IO ()
listProjects = do
  todos <- readTodoTxt
  let pjs = LA.listProjects todos
  TIO.putStr $ serialize pjs

listInbox :: IO ()
listInbox = do
  todos <- readTodoTxt
  today <- TD.getCurrentDate
  let inbox = IDL.filter (T.isInInbox today) todos
  displayTodos inbox

add :: T.Text -> IO ()
add todoText = do
  putStrLn "==> Adding todo(s):"
  d <- TD.getCurrentDate
  let todos :: IDedList T.Todo = parseOnly' parser todoText
  let datedTodos = fmap (T.addCreationDateIfNonePresent d) todos
  TIO.putStr $ IDL.displayBare datedTodos
  appendToTodoTxt datedTodos

complete :: [ID] -> IO ()
complete targets = do
  putStrLn $ "==> Completing todo(s): " <> show targets
  when (length targets > 3) (putStrLn "You are a machine!!")
  d <- TD.getCurrentDate
  updateTodoFileWith (LA.complete d) targets

uncomplete :: [ID] -> IO ()
uncomplete targets = do
  putStrLn $ "==> Hell yea! Officially reinstaing (un-completing) todo(s): " <> show targets
  updateTodoFileWith LA.uncomplete targets

prioritise :: Char -> [ID] -> IO ()
prioritise pri targets = do
  putStrLn $ "==> Prioritizing todo(s): " <> show targets
  updateTodoFileWith (LA.prioritise . Char.toUpper $ pri) targets

unprioritise :: [ID] -> IO ()
unprioritise targets = do
  putStrLn $ "==> Un-Prioritizing todo(s): " <> show targets
  updateTodoFileWith LA.unprioritise targets

prepend :: T.Text -> [ID] -> IO ()
prepend txt targets = do
  let msg = "==> Prepending \"" <> txt <> "\" to todo(s): " <> serialize targets
  TIO.putStrLn msg
  updateTodoFileWith (LA.prepend txt) targets

append :: T.Text -> [ID] -> IO ()
append txt targets = do
  let msg = "==> Appending \"" <> txt <> "\" to todo(s): " <> serialize targets
  TIO.putStrLn msg
  updateTodoFileWith (LA.append txt) targets

remove :: [ID] -> IO ()
remove targets = do
  let msg = "==> Removing todo(s): " <> serialize targets
  TIO.putStrLn msg
  TIO.putStrLn "They weren't that important anyway"
  updateTodoFileWith LA.delete targets

archive :: Bool -> IO ()
archive pirateOn = do
  todos <- readTodoTxt
  when pirateOn (TIO.putStrLn pirateArchiveMsg)
  let (newTodoTxt, doneTodos) = LA.archive todos
  if null doneTodos
    then TIO.putStrLn $ paint Red "==> Nothing to archive!"
    else do
      putStrLn "==> Archiving todos"
      d <- TD.getCurrentDate
      let datedDoneTodos = fmap (T.addCompletionDateIfNonePresent d) doneTodos
      appendToDoneTxt datedDoneTodos
      writeTodoTxt newTodoTxt
      putStrLn $ show (length datedDoneTodos) <> " todo(s) archived:"
      TIO.putStr $ IDL.displayBare datedDoneTodos
  where
    pirateArchiveMsg =
      paint Yellow "☠ ☠ [Pirate Mode Enabled] ☠ ☠\n"
        <> oldSalt
        <> "Aye aye sir.\n"
        <> oldSalt
        <> "You heard the Captain! Batten down the hatches, ye miserable scallywags\n"
      where
        oldSalt = paint Yellow "old-salt: "

clean :: IO ()
clean = do
  _ <- archive False
  putStrLn "==> Sorting and clearing whitespace"
  todos <- readTodoTxt
  writeTodoTxt . IDL.compact $ todos

edit :: IO ()
edit = do
  editor <- Env.getEnv "EDITOR"
  putStrLn $ "==> Editing todo.txt with $EDITOR (" <> editor <> ")"
  putStrLn "Mr. Sulu, you have the conn."
  todoTxt <- getTodoTxtFilePath
  Process.callProcess editor [todoTxt]

main :: IO ()
main = do
  args <- Env.getArgs
  if null args
    then list False
    else do
      opts <- execParser CLI.optsParser
      dispatch opts

dispatch :: CLI.Cmd -> IO ()
dispatch opts = case opts of
  (CLI.Add txt) -> add txt
  (CLI.Append txt ids) -> append txt ids
  (CLI.Archive pirateOn) -> archive pirateOn
  (CLI.Clean) -> clean
  (CLI.Complete ids) -> complete ids
  (CLI.Edit) -> edit
  (CLI.List showAll) -> list showAll
  (CLI.Prepend txt ids) -> prepend txt ids
  (CLI.Prioritise pri ids) -> prioritise pri ids
  (CLI.Remove ids) -> remove ids
  (CLI.Uncomplete ids) -> uncomplete ids
  (CLI.Unprioritise ids) -> unprioritise ids
  (CLI.ListContexts) -> listContexts
  (CLI.ListProjects) -> listProjects
  (CLI.ListInbox) -> listInbox

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
updateTodoFileWith :: LA.ListUpdateFn -> [ID] -> IO ()
updateTodoFileWith f targetIDs = do
  todos <- readTodoTxt
  let todos' = f targetIDs todos
  let affected = IDL.getElems targetIDs todos
  if null affected
    then do
      displayTodos todos
      TIO.putStrLn $ paint Red $ "Could not find todo(s): " <> serialize targetIDs
    else do
      writeTodoTxt todos'
      TIO.putStr $ "Todo(s) affected:\n" <> IDL.displayBare affected
